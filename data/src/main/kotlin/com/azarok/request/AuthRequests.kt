package com.azarok.request

import com.google.gson.annotations.SerializedName

/**
 * Created by pogosazaryan on 06/01/2019.
 *      p.azaryan@fasten.com
 *      Last edit by pogosazaryan on 06/01/2019
 */
data class RegisterRequest(
        @SerializedName("username")
        private val username: String,
        @SerializedName("password")
        private val password: String
)