package com.azarok.repository

import com.azarok.api.AppApi
import com.azarok.await
import com.azarok.base.CallResult
import com.azarok.base.auth.AuthRepository
import com.azarok.request.RegisterRequest
import com.azarok.safeCall
import com.azarok.source.TokenManager
import javax.inject.Inject

/**
 * Created by pogosazaryan on 03/01/2019.
 *      p.azaryan@fasten.com
 *      Last edit by pogosazaryan on 03/01/2019
 */
class AuthRepositoryImpl @Inject constructor(
        private val tokenManager: TokenManager,
        private val api: AppApi
) : AuthRepository {
    override suspend fun registerUser(username: String, password: String): CallResult<String> {
        return safeCall {
            val result = api.register(RegisterRequest(username, password)).await()
            tokenManager.saveToken(result.token)
            result.token
        }
    }
}