package com.azarok.repository

import com.azarok.base.session.SessionRepository
import com.azarok.source.TokenManager
import javax.inject.Inject

class SessionRepositoryImpl @Inject constructor(
        private val tokenManager: TokenManager
) : SessionRepository {
    override val token
        get() = tokenManager.getToken()
}