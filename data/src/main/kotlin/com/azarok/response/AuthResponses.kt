package com.azarok.response

import com.google.gson.annotations.SerializedName

/**
 * Created by pogosazaryan on 06/01/2019.
 *      p.azaryan@fasten.com
 *      Last edit by pogosazaryan on 06/01/2019
 */
data class RegisterResponse(
        @SerializedName("token")
        val token: String
)