package com.azarok

import com.azarok.base.CallResult
import kotlinx.coroutines.suspendCancellableCoroutine
import retrofit2.Call
import retrofit2.Callback
import retrofit2.HttpException
import retrofit2.Response
import timber.log.Timber
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException

/**
 * Created by pogosazaryan on 06/01/2019.
 *      p.azaryan@fasten.com
 *      Last edit by pogosazaryan on 06/01/2019
 */
inline fun <T> safeCall(
        block: () -> T
): CallResult<T> {
    return try {
        CallResult.Success(block.invoke())
    } catch (ex: Exception) {
        CallResult.Error(ex)
    }
}

suspend fun <T : Any> Call<T>.await(): T = suspendCancellableCoroutine {
    enqueue(object : Callback<T> {
        override fun onResponse(call: Call<T>, response: Response<T>) {
            if (response.isSuccessful) {
                it.resume(response.body()!!)
            } else {
                it.resumeWithException(HttpException(response))
            }
        }

        override fun onFailure(call: Call<T>, t: Throwable) {
            if (it.isCancelled) return
            it.resumeWithException(t)
        }
    })

    it.invokeOnCancellation {
        try {
            cancel()
        } catch (ex: Throwable) {
            Timber.e(ex)
        }
    }
}