package com.azarok.source

import android.content.Context

/**
 * Created by pogosazaryan on 06/01/2019.
 *      p.azaryan@fasten.com
 *      Last edit by pogosazaryan on 06/01/2019
 */
class TokenManager(context: Context) {
    private val prefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)

    fun saveToken(token: String) {
        prefs.edit().putString(PREF_TOKEN, token).apply()
    }

    fun getToken(): String = prefs.getString(PREF_TOKEN, "")!!

    private companion object {
        const val PREF_TOKEN = "PREF_TOKEN"
        const val PREFS_NAME = "TokenManager"
    }
}