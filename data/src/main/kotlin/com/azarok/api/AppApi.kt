package com.azarok.api

import com.azarok.request.RegisterRequest
import com.azarok.response.RegisterResponse
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

/**
 * Created by pogosazaryan on 03/01/2019.
 *      p.azaryan@fasten.com
 *      Last edit by pogosazaryan on 03/01/2019
 */
interface AppApi {
    @POST("/register")
    fun register(@Body request: RegisterRequest): Call<RegisterResponse>
}