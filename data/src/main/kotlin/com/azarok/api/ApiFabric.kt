package com.azarok.api

import com.azarok.source.TokenManager
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import timber.log.Timber

/**
 * Created by pogosazaryan on 03/01/2019.
 *      p.azaryan@fasten.com
 *      Last edit by pogosazaryan on 03/01/2019
 */
object ApiFabric {
    private const val LOG_TAG = "OkHttp"
    private const val API_URL = "http://192.168.1.109:8080"

    fun createApi(tokenManager: TokenManager): AppApi {
        val authInterceptor = AuthorizationInterceptor(tokenManager)
        val loggingInterceptor = HttpLoggingInterceptor {
            Timber.tag(LOG_TAG).d(it)
        }.setLevel(HttpLoggingInterceptor.Level.BODY)

        val client = OkHttpClient.Builder()
                .addInterceptor(authInterceptor)
                .addInterceptor(loggingInterceptor)
                .build()

        return Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .baseUrl(API_URL)
                .build()
                .create(AppApi::class.java)
    }
}