package com.azarok.api

import com.azarok.source.TokenManager
import okhttp3.Interceptor
import okhttp3.Response

/**
 * Created by pogosazaryan on 06/01/2019.
 *      p.azaryan@fasten.com
 *      Last edit by pogosazaryan on 06/01/2019
 */
class AuthorizationInterceptor(private val tokenManager: TokenManager): Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val token = tokenManager.getToken()
        if (token.isEmpty()) {
            return chain.proceed(chain.request())
        }

        var request = chain.request()
        request = request.newBuilder()
                .addHeader("Authorization", "Bearer $token")
                .build()

        return chain.proceed(request)
    }
}