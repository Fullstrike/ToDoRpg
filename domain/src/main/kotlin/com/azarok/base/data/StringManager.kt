package com.azarok.base.data

interface StringManager {
    fun emptyLoginError(): String

    fun emptyPasswordError(): String

    fun emptySmallPasswordError(): String

    fun unknownError(): String
}