package com.azarok.base.auth

import com.azarok.base.CallResult
import com.azarok.base.data.StringManager
import com.azarok.base.exception.LocalizedException

class AuthUseCase(
        private val authRepository: AuthRepository,
        private val stringManager: StringManager
) {
    suspend fun registerUser(username: String, password: String): CallResult<String> {
        if (username.isEmpty()) {
            return CallResult.Error(LoginException(stringManager.emptyLoginError()))
        }
        if (password.isEmpty()) {
            return CallResult.Error(PasswordException(stringManager.emptyPasswordError()))
        }
        if (password.length < MIN_PASSWORD_LENGTH) {
            return CallResult.Error(PasswordException(stringManager.emptySmallPasswordError()))
        }

        return authRepository.registerUser(username, password)
    }

    companion object {
        private const val MIN_PASSWORD_LENGTH = 6
    }

    class LoginException(message: String = "") : LocalizedException(message)

    class PasswordException(message: String = "") : LocalizedException(message)
}