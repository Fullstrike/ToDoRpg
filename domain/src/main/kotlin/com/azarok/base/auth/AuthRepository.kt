package com.azarok.base.auth

import com.azarok.base.CallResult

interface AuthRepository {
    suspend fun registerUser(username: String, password: String): CallResult<String>
}