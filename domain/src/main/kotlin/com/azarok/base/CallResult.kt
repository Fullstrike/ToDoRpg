package com.azarok.base

/**
 * Created by pogosazaryan on 06/01/2019.
 *      p.azaryan@fasten.com
 *      Last edit by pogosazaryan on 06/01/2019
 */
sealed class CallResult<out T> {
    data class Success<out T>(val data: T): CallResult<T>()
    data class Error(val exception: Exception): CallResult<Nothing>()
}