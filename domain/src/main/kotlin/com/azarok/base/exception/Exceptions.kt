package com.azarok.base.exception

class InternalException(message: String = "") : LocalizedException(message)