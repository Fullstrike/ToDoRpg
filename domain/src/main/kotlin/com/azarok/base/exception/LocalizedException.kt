package com.azarok.base.exception

open class LocalizedException(
        private val localizedMessage: String = "",
        description: String = ""
) : Exception(description) {
    override fun getLocalizedMessage(): String = localizedMessage
}