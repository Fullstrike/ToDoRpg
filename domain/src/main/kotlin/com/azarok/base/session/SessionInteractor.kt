package com.azarok.base.session

class SessionInteractor(private val sessionRepository: SessionRepository) {
    val isUserAuthed
        get() = sessionRepository.token.isNotEmpty()
}