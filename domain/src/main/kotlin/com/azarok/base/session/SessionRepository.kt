package com.azarok.base.session

interface SessionRepository {
    val token: String
}