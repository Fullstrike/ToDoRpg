package com.azarok.presentation.base

import android.app.Activity
import android.os.Bundle
import android.widget.Toast
import androidx.fragment.app.FragmentActivity
import com.azarok.di.MainComponentHolder

/**
 * Created by pogosazaryan on 06.07.2018.
 *      p.azaryan@fasten.com
 *      Last edit by pogosazaryan on 06.07.2018
 */
abstract class BaseActivity : FragmentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        MainComponentHolder.inject(this)
        super.onCreate(savedInstanceState)
    }

    override fun onDestroy() {
        super.onDestroy()
        if (isFinishing) {
            MainComponentHolder.destroy(this)
        }
    }
}

fun Activity.showToast(message: String) {
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
}