package com.azarok.presentation.base

import com.azarok.R
import com.azarok.di.navigation.FlowNavigationQualifier
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.android.support.SupportAppNavigator
import javax.inject.Inject

abstract class FlowFragment : BaseFragment() {
    @Inject
    @field:FlowNavigationQualifier
    lateinit var navigatorHolder: NavigatorHolder
    protected val navigator by lazy {
        SupportAppNavigator(activity, childFragmentManager, R.id.fragmentContainer)
    }

    override fun onResume() {
        super.onResume()
        navigatorHolder.setNavigator(navigator)
    }

    override fun onPause() {
        navigatorHolder.removeNavigator()
        super.onPause()
    }
}