package com.azarok.presentation.base

import android.content.Context
import androidx.fragment.app.Fragment
import com.azarok.di.MainComponentHolder

/**
 * Created by pogosazaryan on 16.09.2018.
 *      p.azaryan@fasten.com
 *      Last edit by pogosazaryan on 16.09.2018
 */
abstract class BaseFragment: Fragment() {
    override fun onAttach(context: Context?) {
        MainComponentHolder.inject(this)
        super.onAttach(context)
    }


    override fun onDetach() {
        super.onDetach()
        MainComponentHolder.destroy(this)
    }
}