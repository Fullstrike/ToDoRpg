package com.azarok.presentation.todo

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.azarok.R
import com.azarok.presentation.base.BaseFragment

class TodoListFragment : BaseFragment() {
    companion object {
        fun newInstance(): TodoListFragment = TodoListFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_todo_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val list = view.findViewById<RecyclerView>(R.id.rvTodo)
        setupList(list)
    }

    private fun setupList(list: RecyclerView) {

    }
}