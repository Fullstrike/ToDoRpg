package com.azarok.presentation.settings

import com.azarok.presentation.base.BaseFragment

class SettingsFragment : BaseFragment() {
    companion object {
        fun newInstance(): SettingsFragment = SettingsFragment()
    }
}