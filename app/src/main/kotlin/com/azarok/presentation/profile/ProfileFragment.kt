package com.azarok.presentation.profile

import com.azarok.presentation.base.BaseFragment

/**
 * Created by pogosazaryan on 16.09.2018.
 *      p.azaryan@fasten.com
 *      Last edit by pogosazaryan on 16.09.2018
 */
class ProfileFragment : BaseFragment() {
    companion object {
        fun newInstance(): ProfileFragment = ProfileFragment()
    }

}