package com.azarok.presentation

import androidx.fragment.app.Fragment
import com.azarok.presentation.login.LoginFragment
import com.azarok.presentation.main.MainFragment
import com.azarok.presentation.profile.ProfileFragment
import com.azarok.presentation.todo.TodoListFragment
import ru.terrakok.cicerone.android.support.SupportAppScreen

object Screens {
    object LoginAppScreen : SupportAppScreen() {
        override fun getFragment(): Fragment = LoginFragment.newInstance()
    }

    object MainAppScreen : SupportAppScreen() {
        override fun getFragment(): Fragment = MainFragment.newInstance()
    }

    object ProfileAppScreen : SupportAppScreen() {
        override fun getFragment(): Fragment = ProfileFragment.newInstance()
    }

    object TodoListAppScreen : SupportAppScreen() {
        override fun getFragment(): Fragment = TodoListFragment.newInstance()
    }

    object SettingsAppScreen : SupportAppScreen() {
        override fun getFragment(): Fragment = TodoListFragment.newInstance()
    }
}