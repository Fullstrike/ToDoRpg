package com.azarok.presentation.login

import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import com.azarok.base.CallResult
import com.azarok.base.auth.AuthUseCase
import com.azarok.base.auth.AuthUseCase.LoginException
import com.azarok.base.auth.AuthUseCase.PasswordException
import com.azarok.navigation.login.LoginCoordinator
import com.azarok.presentation.base.BaseViewModel
import com.azarok.utils.view.Event
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

class LoginViewModel @Inject constructor(
        private val authUseCase: AuthUseCase,
        private val loginCoordinator: LoginCoordinator
): BaseViewModel() {
    val viewData = LoginViewData()
    fun onButtonClicked() {
        launch {
            val username = viewData.login.get() ?: ""
            val password = viewData.password.get() ?: ""

            val result = authUseCase.registerUser(username, password)
            withContext(Dispatchers.Main) {
                when (result) {
                    is CallResult.Success -> loginCoordinator.onLoginSuccess()
                    is CallResult.Error -> {
                        when(result.exception) {
                            is LoginException -> viewData.loginError.set(result.exception.localizedMessage)
                            is PasswordException -> viewData.passwordError.set(result.exception.localizedMessage)
                            else -> viewData.showErrorMessage(result.exception.localizedMessage)
                        }
                    }
                }
            }
        }
    }
}

class LoginViewData {
    val login = object : ObservableField<String>() {
        override fun set(value: String?) {
            super.set(value)
            loginError.set("")
        }
    }
    val loginError = object : ObservableField<String>() {
        override fun set(value: String?) {
            super.set(value)
            loginErrorEnabled.set(!value.isNullOrEmpty())
        }
    }
    val loginErrorEnabled = ObservableBoolean()

    val password = object : ObservableField<String>() {
        override fun set(value: String?) {
            super.set(value)
            passwordError.set("")
        }
    }
    val passwordError = object : ObservableField<String>() {
        override fun set(value: String?) {
            super.set(value)
            passwordErrorEnabled.set(!value.isNullOrEmpty())
        }
    }
    val passwordErrorEnabled = ObservableBoolean()

    val loginEvents = MutableLiveData<Event<LoginViewEvents>>()

    fun showErrorMessage(message: String?) {
        loginEvents.value = Event(LoginViewEvents.ShowMessage(message))
    }
}

sealed class LoginViewEvents {
    class ShowMessage(val message: String?): LoginViewEvents()
}