package com.azarok.presentation.login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.azarok.R
import com.azarok.databinding.FragmentLoginBinding
import com.azarok.presentation.base.BaseFragment
import com.azarok.utils.view.Event
import javax.inject.Inject

/**
 * Created by pogosazaryan on 08/01/2019.
 *      p.azaryan@fasten.com
 *      Last edit by pogosazaryan on 08/01/2019
 */
class LoginFragment : BaseFragment() {
    companion object {
        const val TAG = "LoginFragment"

        fun newInstance() = LoginFragment()
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var model: LoginViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        model = ViewModelProviders.of(this, viewModelFactory).get(LoginViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = DataBindingUtil.inflate<FragmentLoginBinding>(inflater, R.layout.fragment_login, container, false)
        binding.viewModel = model

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        model.viewData.loginEvents.observe(this, Observer<Event<LoginViewEvents>> { event ->
            event?.getContentIfNotHandled()?.let {
                when(it) {
                    is LoginViewEvents.ShowMessage -> Toast.makeText(context, it.message, Toast.LENGTH_SHORT).show()
                }
            }
        })
    }
}