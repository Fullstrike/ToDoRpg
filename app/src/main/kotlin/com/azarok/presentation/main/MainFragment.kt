package com.azarok.presentation.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.azarok.R
import com.azarok.navigation.main.MainCoordinator
import com.azarok.presentation.base.FlowFragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import javax.inject.Inject

/**
 * Created by pogosazaryan on 08/01/2019.
 *      p.azaryan@fasten.com
 *      Last edit by pogosazaryan on 08/01/2019
 */
class MainFragment : FlowFragment() {

    @Inject
    lateinit var mainCoordinator: MainCoordinator

    companion object {
        fun newInstance(): MainFragment {
            return MainFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.findViewById<BottomNavigationView>(R.id.bottomNavigation)
                .setOnNavigationItemSelectedListener {
                    when(it.itemId) {
                        R.id.action_profile -> mainCoordinator.profileScreen()
                        R.id.action_quests -> mainCoordinator.todoListScreen()
                        R.id.action_settings -> mainCoordinator.settingsScreen()
                    }
                    return@setOnNavigationItemSelectedListener true
                }
    }

    override fun onResume() {
        super.onResume()
        if (childFragmentManager.fragments.isEmpty()) {
            mainCoordinator.onColdStart()
        }
    }
}