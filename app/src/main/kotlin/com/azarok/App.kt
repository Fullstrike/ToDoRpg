package com.azarok

import android.app.Application
import com.azarok.di.MainComponentHolder
import com.azarok.utils.logging.LoggingTree
import timber.log.Timber

/**
 * Created by pogosazaryan on 16.09.2018.
 *      p.azaryan@fasten.com
 *      Last edit by pogosazaryan on 16.09.2018
 */
class App: Application() {

    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG) {
            Timber.plant(LoggingTree())
        }

        initDagger()
    }

    private fun initDagger() {
        MainComponentHolder.initAppComponent(this)
    }
}