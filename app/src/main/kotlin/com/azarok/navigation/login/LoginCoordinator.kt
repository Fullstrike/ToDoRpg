package com.azarok.navigation.login

import com.azarok.presentation.Screens
import ru.terrakok.cicerone.Router

class LoginCoordinator(private val router: Router) {
    fun onLoginSuccess() {
        router.newRootScreen(Screens.MainAppScreen)
    }
}