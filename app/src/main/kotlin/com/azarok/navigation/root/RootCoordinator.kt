package com.azarok.navigation.root

import com.azarok.base.session.SessionInteractor
import com.azarok.presentation.Screens
import ru.terrakok.cicerone.Router

class RootCoordinator(
        private val sessionInteractor: SessionInteractor,
        private val router: Router
) {
    fun onColdStart() {
        if (sessionInteractor.isUserAuthed) {
            router.newRootScreen(Screens.MainAppScreen)
        } else {
            router.newRootScreen(Screens.LoginAppScreen)
        }
    }
}