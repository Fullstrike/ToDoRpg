package com.azarok.navigation.main

import com.azarok.navigation.FlowRouter
import com.azarok.presentation.Screens

class MainCoordinator(private val router: FlowRouter) {
    fun onColdStart() {
        router.newRootScreen(Screens.ProfileAppScreen)
    }

    fun profileScreen() {
        router.newRootScreen(Screens.ProfileAppScreen)
    }

    fun todoListScreen() {
        router.newRootScreen(Screens.TodoListAppScreen)
    }

    fun settingsScreen() {
        router.newRootScreen(Screens.SettingsAppScreen)
    }
}