package com.azarok.di

import androidx.lifecycle.ViewModelProvider
import com.azarok.utils.ViewModelFactory
import dagger.Binds
import dagger.Module

@Module
interface ModelModule {
    @Binds
    fun provideViewModelFactory(viewModelFactory: ViewModelFactory): ViewModelProvider.Factory
}