package com.azarok.di

import com.azarok.App
import com.azarok.di.login.LoginComponentHolder
import com.azarok.di.main.MainComponentHolder
import com.azarok.di.root.RootComponentHolder
import com.azarok.presentation.login.LoginFragment
import com.azarok.presentation.main.MainFragment
import com.azarok.presentation.root.RootActivity

object MainComponentHolder {
    private lateinit var appComponent: AppComponent
    private lateinit var componentsMap: Map<String, ComponentHolder>

    fun initAppComponent(app: App) {
        appComponent = DaggerAppComponent.builder()
                .application(app)
                .build()

        initComponentHolders()
    }

    private fun initComponentHolders() {
        val rootComponentHolder = RootComponentHolder(appComponent)
        componentsMap = mapOf(
                RootActivity::class.java.canonicalName to rootComponentHolder,
                LoginFragment::class.java.canonicalName to LoginComponentHolder(rootComponentHolder),
                MainFragment::class.java.canonicalName to MainComponentHolder(rootComponentHolder)
        )
    }

    fun inject(any: Any) {
        componentsMap[any::class.java.canonicalName!!]?.inject(any)
    }

    fun destroy(any: Any) {
        componentsMap[any::class.java.canonicalName!!]?.destroy(any)
    }
}