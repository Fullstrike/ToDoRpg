package com.azarok.di.navigation

import javax.inject.Qualifier

@Qualifier
@Retention(AnnotationRetention.RUNTIME)
annotation class FlowNavigationQualifier {
}