package com.azarok.di.navigation

import com.azarok.di.PerActivity
import com.azarok.navigation.FlowRouter
import dagger.Module
import dagger.Provides
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router

@Module
class FlowNavigationModule {
    @Provides
    @PerActivity
    fun provideFlowCicerone(router: Router): Cicerone<FlowRouter> {
        return Cicerone.create(FlowRouter(router))
    }

    @Provides
    @PerActivity
    @FlowNavigationQualifier
    fun provideFlowNavigatorHolder(cicerone: Cicerone<FlowRouter>): NavigatorHolder {
        return cicerone.navigatorHolder
    }

    @Provides
    @PerActivity
    fun provideFlowRouter(cicerone: Cicerone<FlowRouter>): FlowRouter {
        return cicerone.router
    }
}