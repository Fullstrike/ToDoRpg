package com.azarok.di.main

import com.azarok.navigation.FlowRouter
import com.azarok.navigation.main.MainCoordinator
import dagger.Module
import dagger.Provides

@Module
class MainModule {
    @Provides
    fun provideMainCoordinator(router: FlowRouter): MainCoordinator {
        return MainCoordinator(router)
    }
}