package com.azarok.di.main

import com.azarok.di.ComponentHolder
import com.azarok.di.root.RootComponentHolder
import com.azarok.presentation.main.MainFragment

class MainComponentHolder(private val rootComponentHolder: RootComponentHolder) : ComponentHolder {
    private var mainComponent : MainComponent? = null
    override fun inject(toMe: Any) {
        when(toMe) {
            is MainFragment -> getComponent().inject(toMe)
        }
    }

    override fun destroy(me: Any) {
        mainComponent = null
    }

    private fun getComponent(): MainComponent {
        if (mainComponent == null) {
            mainComponent = rootComponentHolder.getComponent()
                    .mainBuilder()
                    .build()
        }

        return mainComponent!!
    }
}