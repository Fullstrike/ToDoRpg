package com.azarok.di

interface ComponentHolder {
    fun inject(toMe: Any)

    fun destroy(me: Any)
}