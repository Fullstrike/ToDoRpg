package com.azarok.di.session

import com.azarok.base.session.SessionInteractor
import com.azarok.base.session.SessionRepository
import com.azarok.repository.SessionRepositoryImpl
import dagger.Binds
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
abstract class SessionModule {
    @Binds
    @Singleton
    abstract fun provideSessionRepository(sessionRepository: SessionRepositoryImpl): SessionRepository

    @Module
    companion object {
        @Provides
        @Singleton
        @JvmStatic
        fun provideSessionInteractor(sessionRepository: SessionRepository): SessionInteractor {
            return SessionInteractor(sessionRepository)
        }
    }
}