package com.azarok.di

import android.content.Context
import com.azarok.api.ApiFabric
import com.azarok.api.AppApi
import com.azarok.source.TokenManager
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class NetworkModule {

    @Provides
    @Singleton
    fun provideTokenManager(context: Context): TokenManager {
        return TokenManager(context)
    }

    @Provides
    @Singleton
    fun provideApi(tokenManager: TokenManager): AppApi {
        return ApiFabric.createApi(tokenManager)
    }
}