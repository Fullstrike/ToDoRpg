package com.azarok.di

import com.azarok.App
import com.azarok.di.navigation.NavigationModule
import com.azarok.di.root.RootComponent
import com.azarok.di.session.SessionModule
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

/**
 * Created by pogosazaryan on 16.09.2018.
 *      p.azaryan@fasten.com
 *      Last edit by pogosazaryan on 16.09.2018
 */
@Singleton
@Component(modules = [
    AppModule::class,
    NetworkModule::class,
    NavigationModule::class,
    SessionModule::class
])
interface AppComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(app: App): Builder

        fun build(): AppComponent
    }

    fun mainBuilder(): RootComponent.Builder
}