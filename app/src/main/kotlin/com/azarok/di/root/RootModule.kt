package com.azarok.di.root

import com.azarok.base.session.SessionInteractor
import com.azarok.navigation.root.RootCoordinator
import dagger.Module
import dagger.Provides
import ru.terrakok.cicerone.Router

@Module
class RootModule {
    @Provides
    fun provideRootCoordinator(
            sessionInteractor: SessionInteractor,
            router: Router
    ): RootCoordinator {
        return RootCoordinator(sessionInteractor, router)
    }
}