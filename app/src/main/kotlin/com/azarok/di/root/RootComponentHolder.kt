package com.azarok.di.root

import com.azarok.di.AppComponent
import com.azarok.di.ComponentHolder
import com.azarok.presentation.root.RootActivity

class RootComponentHolder(private val appComponent: AppComponent): ComponentHolder {
    private var rootComponent: RootComponent? = null

    override fun inject(toMe: Any) {
        when(toMe) {
            is RootActivity -> getComponent().inject(toMe)
        }
    }

    override fun destroy(me: Any) {
        rootComponent = null
    }

    fun getComponent(): RootComponent {
        if (rootComponent == null) {
            rootComponent = appComponent.mainBuilder()
                    .build()
        }

        return rootComponent!!
    }
}