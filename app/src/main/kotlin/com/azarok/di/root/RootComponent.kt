package com.azarok.di.root

import com.azarok.di.PerActivity
import com.azarok.di.login.LoginComponent
import com.azarok.di.main.MainComponent
import com.azarok.di.navigation.FlowNavigationModule
import com.azarok.presentation.root.RootActivity
import dagger.Subcomponent

@Subcomponent(modules = [RootModule::class, FlowNavigationModule::class])
@PerActivity
interface RootComponent {
    @Subcomponent.Builder
    interface Builder {
        fun build(): RootComponent
    }

    fun loginBuilder(): LoginComponent.Builder

    fun mainBuilder(): MainComponent.Builder

    fun inject(activity: RootActivity)
}