package com.azarok.di.login

import com.azarok.di.ComponentHolder
import com.azarok.di.root.RootComponentHolder
import com.azarok.presentation.login.LoginFragment

class LoginComponentHolder(private val rootComponentHolder: RootComponentHolder) : ComponentHolder {
    private var loginComponent: LoginComponent? = null

    override fun inject(toMe: Any) {
        when (toMe) {
            is LoginFragment -> getComponent().inject(toMe)
        }
    }

    override fun destroy(me: Any) {
        loginComponent = null
    }

    private fun getComponent(): LoginComponent {
        if (loginComponent == null) {
            loginComponent = rootComponentHolder.getComponent()
                    .loginBuilder()
                    .build()
        }

        return loginComponent!!
    }
}