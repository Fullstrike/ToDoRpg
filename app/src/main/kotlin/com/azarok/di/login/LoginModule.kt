package com.azarok.di.login

import androidx.lifecycle.ViewModel
import com.azarok.di.ViewModelKey
import com.azarok.navigation.login.LoginCoordinator
import com.azarok.presentation.login.LoginViewModel
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import ru.terrakok.cicerone.Router

@Module
abstract class LoginModule {
    @Binds
    @IntoMap
    @ViewModelKey(LoginViewModel::class)
    abstract fun provideLoginViewModel(viewModel: LoginViewModel): ViewModel


    @Module
    companion object {
        @Provides
        @JvmStatic
        fun provideLoginCoordinator(router: Router): LoginCoordinator {
            return LoginCoordinator(router)
        }
    }
}