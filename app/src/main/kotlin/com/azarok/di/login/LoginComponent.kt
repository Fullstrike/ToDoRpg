package com.azarok.di.login

import com.azarok.di.ModelModule
import com.azarok.presentation.login.LoginFragment
import dagger.Subcomponent

@Subcomponent(modules = [LoginModule::class, AuthModule::class, ModelModule::class])
interface LoginComponent {
    @Subcomponent.Builder
    interface Builder {
        fun build(): LoginComponent
    }

    fun inject(fragment: LoginFragment)
}