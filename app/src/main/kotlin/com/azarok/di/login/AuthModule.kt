package com.azarok.di.login

import com.azarok.base.auth.AuthRepository
import com.azarok.base.auth.AuthUseCase
import com.azarok.base.data.StringManager
import com.azarok.repository.AuthRepositoryImpl
import dagger.Binds
import dagger.Module
import dagger.Provides

@Module
abstract class AuthModule {
    @Binds
    abstract fun provideAuthRepository(authRepositoryImpl: AuthRepositoryImpl): AuthRepository

    @Module
    companion object {
        @Provides
        @JvmStatic
        fun provideAuthUseCase(
                authRepository: AuthRepository,
                stringManager: StringManager
        ): AuthUseCase {
            return AuthUseCase(authRepository, stringManager)
        }
    }
}