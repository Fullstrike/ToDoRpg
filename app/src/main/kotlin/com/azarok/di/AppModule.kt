package com.azarok.di

import android.content.Context
import com.azarok.App
import com.azarok.base.data.StringManager
import com.azarok.utils.view.StringManagerImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by pogosazaryan on 16.09.2018.
 *      p.azaryan@fasten.com
 *      Last edit by pogosazaryan on 16.09.2018
 */
@Module
class AppModule {
    @Provides
    @Singleton
    fun provideContext(app: App): Context {
        return app
    }

    @Provides
    @Singleton
    fun provideResourceManager(context: Context): StringManager {
        return StringManagerImpl(context)
    }
}