package com.azarok.utils.logging

import android.annotation.SuppressLint
import android.util.Log
import timber.log.Timber

/**
 * Created by pogosazaryan on 16.09.2018.
 *      p.azaryan@fasten.com
 *      Last edit by pogosazaryan on 16.09.2018
 */
class LoggingTree: Timber.DebugTree() {
    @SuppressLint("LogNotTimber")
    override fun log(priority: Int, tag: String?, message: String, t: Throwable?) {
        when(priority) {
            Log.ASSERT -> Log.wtf(tag, message, t)
            Log.DEBUG -> Log.d(tag, message, t)
            Log.INFO -> Log.i(tag, message, t)
            Log.ERROR -> Log.e(tag, message, t)
            Log.WARN -> Log.w(tag, message, t)
            Log.VERBOSE -> Log.v(tag, message, t)
            else -> Log.i(tag, message, t)
        }
    }
}