package com.azarok.utils.view

import android.content.Context
import androidx.annotation.StringRes
import com.azarok.R
import com.azarok.base.data.StringManager

class StringManagerImpl(private val context: Context): StringManager {

    override fun unknownError(): String = getString(R.string.unknown_error)

    override fun emptyLoginError(): String = getString(R.string.auth_error_empty_login)

    override fun emptyPasswordError(): String = getString(R.string.auth_error_empty_password)

    override fun emptySmallPasswordError(): String = getString(R.string.auth_error_small_password)

    private fun getString(@StringRes stringRes: Int): String {
        return context.getString(stringRes)
    }
}